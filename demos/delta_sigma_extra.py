from cosmosis.postprocessing.cosmology_theory_plots import Plot,plot_list
from cosmosis.postprocessing import lazy_pylab as pylab
import numpy as np

#Since we are just calculating the surface density with the tester sampler,
#We need to put our plot directly into the cosmology_theory_plots
#list by making our plot a subclass of Plot.
class delta_sigma_Plot(Plot):
    filename='delta_sigma'
    def plot(self):
        super(delta_sigma_Plot,self).plot()

        #Plot the cosmosis stuff
        z = self.load_file("distances","z")
        r = self.load_file("setup_delta_sigma","radii")
        delta_sigma = self.load_file("delta_sigma","delta_sigma")
        delta_sigma = delta_sigma.reshape((len(z),len(r)))
        
        #Loop over all redshifts and plot delta_sigma(R,z)
        for i in range(len(z)):
            pylab.loglog(r,r*delta_sigma[i],label="z="+str(z[i]))
        #Make the plot pretty
        axes = pylab.gca()
        pylab.xlabel(r"$R$ $h^{-1}Mpc$")
        pylab.ylabel(r"$R\Delta\Sigma(R,z)$ $M_\odot Mpc/pc^{-2}$")
        pylab.legend(loc="upper left")
        pylab.grid(True)



