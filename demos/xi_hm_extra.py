from cosmosis.postprocessing.cosmology_theory_plots import Plot,plot_list
from cosmosis.postprocessing import lazy_pylab as pylab
import numpy as np

#Since this extra should be used with the test sampler
#we have to inherit from Plot.
class xi_hm_Plot(Plot):
    filename='xi_hm'
    def plot(self):
        super(xi_hm_Plot,self).plot()

        #Plot the cosmosis stuff
        z = self.load_file("distances","z")
        r = self.load_file("setup_delta_sigma","radii")
        xi_hm = self.load_file("xi_hm","xi_hm")
        xi_hm = xi_hm.reshape((len(z),len(r)))
        
        #Loop over all redshifts and plot xi_hm(R,z)
        for i in range(len(z)):
            pylab.loglog(r,r*r*xi_hm[i],label="z="+str(z[i]))
        #Make the plot pretty
        axes = pylab.gca()
        pylab.xlabel(r"$R$ $h^{-1}Mpc$")
        pylab.ylabel(r"$R^2\Xi_hm(R,z)$ $Mpc^2h^{-2}$")
        pylab.legend(loc="upper left")
        pylab.grid(True)



